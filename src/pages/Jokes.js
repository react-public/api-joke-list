import JokeList from "../components/jokes/JokeList";
import {Fragment, useEffect} from "react";
import useHttp from "../hooks/use-http";
import {getJokes} from "../utils/firebase-api";
import Loader from "../components/UI/Loader";
import NoJokesFound from "../components/jokes/NoJokesFound";

// const DUMMY_JOKES = [
//     {
//         id: 1,
//         topic: 'Programming',
//         text: 'Text text...',
//     },
//     {
//         id: 2,
//         topic: 'Joke',
//         text: 'Bla bla bla',
//     }
// ];

const Joke = () => {
    const {
        sendHttpRequest,
        status,
        data: loadedJokes,
        error,
    } = useHttp(getJokes, true);

    useEffect(() => {
        sendHttpRequest();
    }, [sendHttpRequest]);

    if (status === 'pending') {
        return <div className={'centered'}>
            <Loader/>
        </div>
    }

    if (error) {
        return <div className={'centered focused'}>
            {error}
        </div>
    }

    if (status === 'completed' && (!loadedJokes || loadedJokes.length === 0)) {
        return <NoJokesFound/>
    }

    return (
        <Fragment>
            <h1>Joke List</h1>
            <JokeList jokes={loadedJokes}/>
        </Fragment>
    );
};

export default Joke;
import {Link, Route, useParams, useRouteMatch} from "react-router-dom";
import {Fragment, useEffect} from 'react';
import Comments from "../components/comments/Comments";
import HighlightedJoke from "../components/jokes/HighlightedJoke";
import useHttp from "../hooks/use-http";
import {getJoke} from "../utils/firebase-api";
import Loader from "../components/UI/Loader";

// const DUMMY_JOKES = [
//     {
//         id: '1',
//         topic: 'Programming',
//         text: 'Text text...',
//     },
//     {
//         id: '2',
//         topic: 'Joke',
//         text: 'Bla bla bla',
//     }
// ];

const JokeDetails = () => {
    const routeMatch = useRouteMatch();
    const params = useParams();
    const {jokeId} = params;

    const {
        sendHttpRequest,
        status,
        data: joke,
        error
    } = useHttp(getJoke, true);
    useEffect(() => {sendHttpRequest(jokeId)}, [sendHttpRequest, jokeId])
    //const joke = DUMMY_JOKES.find((joke) => joke.id === params.jokesId);

    if (status === 'pending') {
        return <div className={'centered'}>
            <Loader/>
        </div>
    }

    if (error) {
        return <p className={'centered'}>{error}</p>
    }

    if (!joke.text || !joke.topic) {
        return <h1 className="centered">Joke not found</h1>
    }

    return (
        <Fragment>
            <HighlightedJoke text={joke.text} topic={joke.topic}/>
            <Route path={routeMatch.path} exact>
                <div className={'centered'}>
                    <Link
                        className={'btn--empty'}
                        to={`${routeMatch.url}/comments`}
                    >
                        Show Comments
                    </Link>
                </div>
            </Route>
            <Route path={`${routeMatch.path}/comments`}>
                <Comments/>
            </Route>
        </Fragment>
    );
};

export default JokeDetails;
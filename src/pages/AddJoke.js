import {Fragment, useEffect} from "react";
import JokeForm from "../components/jokes/JokeForm";
import {useHistory} from "react-router-dom";
import useHttp from "../hooks/use-http";
import {addJoke} from "../utils/firebase-api";

const AddJoke = () => {
    const history = useHistory();
    const {sendHttpRequest, httpState} = useHttp(addJoke);
    console.log(httpState);
    useEffect(() => {
        if (httpState === 'completed') {
            history.push('/joke-list');
        }
    },[httpState, history]);

    const addJokeHandler = (jokeData) => {
        sendHttpRequest(jokeData);
    };

    return (
        <Fragment>
            <h1>Joke Add</h1>
            <JokeForm onAddJoke={addJokeHandler} isLoading={httpState === 'pending'}/>
        </Fragment>
    );
};

export default AddJoke;
import {Redirect, Route, Switch} from "react-router-dom";
import Header from "./components/layout/Header";
import Jokes from "./pages/Jokes";
import JokeDetails from "./pages/JokeDetails";
import AddJoke from "./pages/AddJoke";
import NotFound from "./pages/NotFound";



function App() {
    return (
        <div>
            <Header/>
            <Switch>
                <Route path={'/'} exact>
                    <Redirect to={'/joke-list'}/>
                </Route>
                <Route path={'/joke-list'} exact>
                    <Jokes/>
                </Route>
                <Route path={'/joke-list/:jokesId'}>
                    <JokeDetails/>
                </Route>
                <Route path={'/add-joke'}>
                    <AddJoke/>
                </Route>
                <Route path='*'>
                    <NotFound/>
                </Route>
            </Switch>
        </div>
    );
}

export default App;
